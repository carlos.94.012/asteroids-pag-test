using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region Singleton
    static GameManager instance;
    public static GameManager Instance { get { return instance; } }


    void Awake()
    {
        instance = this;
    }
    #endregion Singleton
    [SerializeField] public ScriptableStats Stats;


    GameObject bulletPreFab;
    List<GameObject> bulletPoolList = new List<GameObject>();
    GameObject bulletObjects;


    GameObject asteroidPreFab;
    List<GameObject> asteroidPoolList = new List<GameObject>();
    GameObject asteroidObjects;

    bool isPlaying = true;
    public bool IsPlaying { get => isPlaying; set => isPlaying = value; }
    [Header(" ")]
    [SerializeField] float respawnTime = 1;
    public float RespawnTime { set => respawnTime = value; }

    int asteroidDestroyed;
    int restarts;

    private void Start()
    {
        Init();
        StartBulletPool();
        StartAsteroidPool();
    }

    void Init()
    {
        //GM
        respawnTime = Stats.RespawnTime;
        bulletPreFab = Stats.BulletPrefab;
        asteroidPreFab = Stats.AsteroidPrefab;

        //Spawn
        SpawnAsteroides.TimeSpawn = Stats.SpawnTime;

        //Player
        Player.Instance.Speed = Stats.Speed;
        Player.Instance.RotateSpeed = Stats.RotationSpeed;
        Player.Instance.DragMovement = Stats.Drag;
        Player.Instance.DragAgular = Stats.DragAngular;
        Player.Instance.FireRate = Stats.FireRate;

    }


    #region Bullets
    void StartBulletPool()
    {
        bulletObjects = new GameObject();
        bulletObjects.name = "Bullets";
        bulletObjects.transform.parent = this.transform;
        CreateBulletPool(5);
    }

    void CreateBulletPool(int ammount)
    {
        for (int i = 0; i < ammount; i++)
        {
            GameObject newBullet = Instantiate(bulletPreFab.gameObject);
            newBullet.SetActive(false);
            bulletPoolList.Add(newBullet);
            newBullet.transform.parent = bulletObjects.transform;
        }
    }

    public GameObject BulletRequest()
    {
        for (int i = 0; i < bulletPoolList.Count; i++)
        {
            if (!bulletPoolList[i].activeSelf)
            {
                bulletPoolList[i].SetActive(true);
                return bulletPoolList[i];
            }
        }
        CreateBulletPool(1);
        bulletPoolList[bulletPoolList.Count - 1].SetActive(true);
        return bulletPoolList[bulletPoolList.Count - 1];
    }

    void RestartBullet()
    {
        foreach (GameObject item in bulletPoolList)
        {
            item.SetActive(false);
        }
    }
    #endregion Bullets

    #region Asteroids
    void StartAsteroidPool()
    {
        asteroidObjects = new GameObject();
        asteroidObjects.name = "Asteroids";
        asteroidObjects.transform.parent = this.transform;
        CreateAsteroidPool(5);
    }

    void CreateAsteroidPool(int ammount)
    {
        for (int i = 0; i < ammount; i++)
        {
            GameObject newAsteroid = Instantiate(asteroidPreFab.gameObject);
            newAsteroid.SetActive(false);
            asteroidPoolList.Add(newAsteroid);
            newAsteroid.transform.parent = asteroidObjects.transform;
        }
    }
    public GameObject AsteroidRequest()
    {
        for (int i = 0; i < asteroidPoolList.Count; i++)
        {
            if (!asteroidPoolList[i].activeSelf)
            {
                asteroidPoolList[i].SetActive(true);
                return asteroidPoolList[i];
            }
        }
        CreateAsteroidPool(1);
        asteroidPoolList[asteroidPoolList.Count - 1].SetActive(true);
        return asteroidPoolList[asteroidPoolList.Count - 1];
    }

    void RestartAsteroids()
    {
        foreach (GameObject item in asteroidPoolList)
        {
            item.SetActive(false);
        }
    }

    public void OnAsteroidDestroy(float sizeAsteroid)
    {
        asteroidDestroyed += (int)(sizeAsteroid * 10);
        StatsManager.Instance.TextPoints(asteroidDestroyed);
    }
    #endregion Asteroids



    public void Lose(GameObject player)
    {
        StartCoroutine(LoseAction(player));
    }

    IEnumerator LoseAction(GameObject player)
    {
        isPlaying = false;
        restarts++;
        StatsManager.Instance.TextRestarts(restarts);
        yield return new WaitForSecondsRealtime(respawnTime);
        asteroidDestroyed = 0;
        StatsManager.Instance.TextPoints(asteroidDestroyed);
        RestartAsteroids();
        RestartBullet();
        isPlaying = true;
        player.SetActive(true);

    }
}
