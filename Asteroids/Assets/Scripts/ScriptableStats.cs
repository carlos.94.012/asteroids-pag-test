using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Stats", menuName = "Stats")]
public class ScriptableStats : ScriptableObject
{
    [Header("Player")]
    [SerializeField] float speed;
    [SerializeField] float rotationSpeed;
    [SerializeField] float drag;
    [SerializeField] float dragAngular;
    [SerializeField] float fireRate;

    public float Speed { get => speed; }
    public float RotationSpeed { get => rotationSpeed; }
    public float Drag { get => drag; }
    public float DragAngular { get => dragAngular; }
    public float FireRate { get => fireRate; }

    [Header("Bullet")]
    [SerializeField] float speedBullet;
    [SerializeField] float sineAmmount;
    [SerializeField] float damageBullet;
    public float SpeedBullet { get => speedBullet; }
    public float SineAmmount { get => sineAmmount; }
    public float DamageBullet { get => damageBullet; }

    [Header("Asteroids")]
    [SerializeField] float speedMax;
    [SerializeField] float speedMin;
    [SerializeField] float sizeVariation;
    [SerializeField] float minSize;
    [SerializeField] float maxRotSpeed;

    public float SpeedMax { get => speedMax; }
    public float SpeedMin { get => speedMin; }
    public float SizeVariation { get => sizeVariation; }
    public float MinSize { get => minSize; }
    public float MaxRotSpeed { get => maxRotSpeed; }


    [Header("Game Manager")]
    [SerializeField] float respawnTime;
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] GameObject asteroidPrefab;
    public float RespawnTime { get => respawnTime; }
    public GameObject BulletPrefab { get => bulletPrefab; }
    public GameObject AsteroidPrefab { get => asteroidPrefab; }

    [Header("Spawn Manager")]
    [SerializeField] float spawnTime;
    public float SpawnTime { get => spawnTime; }

}
