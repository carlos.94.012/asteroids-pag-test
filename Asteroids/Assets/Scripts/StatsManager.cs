using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StatsManager : MonoBehaviour
{
    #region Singleton
    static StatsManager instance;
    public static StatsManager Instance { get { return instance; } }


    void Awake()
    {
        instance = this;
    }
    #endregion Singleton

    [SerializeField] TMP_Text txtRestarts;
    [SerializeField] TMP_Text txtPoints;


    public void TextRestarts(int val)
    {
        txtRestarts.text = "Restarts: " + val;
    }

    public void TextPoints(int val)
    {
        txtPoints.text = "Points: " + val;
    }

}
