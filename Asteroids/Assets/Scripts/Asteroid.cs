using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour
{

    [SerializeField, Min(0.02f)] float speedMax;
    [SerializeField, Min(0.01f)] float speedMin;
    [SerializeField] float sizeVariation;
    [SerializeField] float minSize;
    [SerializeField] float maxRotationSpeed;

    float hp;
    Rigidbody2D rb;
    Vector3 posInicial;
    bool canDestroy = false;


    void Awake()
    {
        Init();
        rb = GetComponent<Rigidbody2D>();
    }

    void Init()
    {
        minSize = GameManager.Instance.Stats.MinSize;
        sizeVariation = GameManager.Instance.Stats.SizeVariation;
        maxRotationSpeed = GameManager.Instance.Stats.MaxRotSpeed;
        speedMax = GameManager.Instance.Stats.SpeedMax;
        speedMin = GameManager.Instance.Stats.SpeedMin;
    }

    private void OnEnable()
    {
        StartCoroutine(StartMove());
    }

    private void OnBecameInvisible()
    {
        if (canDestroy)
        {
            this.gameObject.SetActive(false);
            canDestroy = false;
        }
    }

    private void OnBecameVisible()
    {
        canDestroy = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bullet"))
        {
            float damage = other.GetComponent<Bullet>().Damage;
            other.gameObject.SetActive(false);
            hp -= damage;
            if (hp <= 0)
            {
                GameManager.Instance.OnAsteroidDestroy(transform.localScale.x);
                this.gameObject.SetActive(false);
            }
        }
    }

    IEnumerator StartMove()
    {
        yield return new WaitForEndOfFrame();

        float randomX = Random.Range(Camera.main.aspect * Camera.main.orthographicSize, -Camera.main.aspect * Camera.main.orthographicSize);
        float randomY = Random.Range(Camera.main.orthographicSize, -Camera.main.orthographicSize);
        Vector3 randomPos = new Vector3(randomX, randomY);
        rb.AddForce(Random.Range(speedMin, speedMax) * (randomPos - transform.position));
        rb.AddTorque(Random.Range(0, maxRotationSpeed) * (Random.Range(0, 2) * 2 - 1));

        float randomSize = Random.Range(1 - sizeVariation, 1 + sizeVariation);
        randomSize = Mathf.Clamp(randomSize, minSize, 1000);
        transform.localScale = (Vector3.one) * randomSize;

        hp = transform.localScale.x * 10;
    }
}
