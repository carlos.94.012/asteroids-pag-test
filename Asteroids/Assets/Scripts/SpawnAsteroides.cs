using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAsteroides : MonoBehaviour
{
    [SerializeField] static float timeSpawn;

    public static float TimeSpawn { get => timeSpawn; set => timeSpawn = value; }

    void Start()
    {
        StartCoroutine(SpawnAsteroid());
    }

    IEnumerator SpawnAsteroid()
    {
        while (true)
        {
            yield return new WaitForSecondsRealtime(TimeSpawn);
            if (GameManager.Instance.IsPlaying)
            {
                GameObject asteroid = GameManager.Instance.AsteroidRequest();
                float posX = Random.Range(-(Camera.main.aspect * Camera.main.orthographicSize * 3), (Camera.main.aspect * Camera.main.orthographicSize * 3)) * (Random.Range(0, 2) * 2 - 1);
                float posY = Random.Range(-(Camera.main.orthographicSize * 3), (Camera.main.orthographicSize * 3)) * (UnityEngine.Random.Range(0, 2) * 2 - 1);
                if (Random.Range(0, 2) == 0) //X
                {
                    asteroid.transform.position = new Vector2(posX, (Random.Range(0, 2) * 2 - 1) * (Camera.main.orthographicSize * 3));
                }
                else
                {
                    asteroid.transform.position = new Vector2((Random.Range(0, 2) * 2 - 1) * (Camera.main.aspect * Camera.main.orthographicSize * 3), posY);
                }
            }
        }

    }
}
