using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

[SelectionBase]
[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    #region Singleton
    static Player instance;
    public static Player Instance { get { return instance; } }



    void Awake()
    {
        instance = this;
    }
    #endregion Singleton

    [Header("MOVEMENT")]
    [SerializeField] float speed;
    [SerializeField] float rotateSpeed;
    [SerializeField] float dragMovement;
    [SerializeField] float dragAgular;
    Rigidbody2D rb;

    public float Speed { get => speed; set => speed = value; }
    public float RotateSpeed { get => rotateSpeed; set => rotateSpeed = value; }
    public float DragMovement { get => dragMovement; set => dragMovement = value; }
    public float DragAgular { get => dragAgular; set => dragAgular = value; }


    [Header("SHOOT SYSTEM")]
    [SerializeField] float fireRate;
    public float FireRate { get => fireRate; set => fireRate = value; }

    bool canShoot = true;
    [SerializeField] Transform canon;

    ScreenZone screenLimits;
    delegate void OnExitScreen();
    static event OnExitScreen OnPlayerExitScreen;


    private void Start()
    {
        screenLimits = ScreenZone.Instance;
        rb = GetComponent<Rigidbody2D>();
        rb.angularDrag = DragAgular;
        rb.drag = DragMovement;
        SetExitScreenInteraction(ExitInteraction.Wall);
    }

    private void Update()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");

        MovePlayer(vertical);
        TurnPlayer(horizontal);

        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Log(screenLimits.BoxCol.size.x / 2);
        }

        if (Input.GetAxisRaw("Fire1") != 0 && canShoot)
        {
            StartCoroutine(Shoot());
        }

    }


    void MovePlayer(float value)
    {
        rb.AddForce(transform.up * Speed * value * Time.deltaTime);
    }

    void TurnPlayer(float value)
    {
        rb.AddTorque(value * RotateSpeed * Time.deltaTime);
    }

    public void SetExitScreenInteraction(ExitInteraction type)
    {
        OnPlayerExitScreen = null;
        switch (type)
        {
            case ExitInteraction.Bumper:
                OnPlayerExitScreen = BumperMode;
                break;
            case ExitInteraction.Wrap:
                OnPlayerExitScreen = WrapMode;
                break;
            case ExitInteraction.Wall:
                OnPlayerExitScreen = null;
                break;
        }
    }
    void BumperMode()
    {
        Vector3 sp = -rb.velocity;
        rb.velocity = Vector3.zero;
        rb.velocity = sp;
    }
    void WrapMode()
    {
        if (transform.position.x > (screenLimits.BoxCol.size.x / 2) || transform.position.x < (screenLimits.BoxCol.size.x / -2))
        {
            transform.position = new Vector2(-transform.position.x, transform.position.y);
        }
        else
        {
            transform.position = new Vector2(transform.position.x, -transform.position.y);
        }
    }


    private void OnEnable()
    {
        transform.position = Vector3.zero;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("ScreenZone"))
        {
            OnPlayerExitScreen?.Invoke();
            return;
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Asteroid"))
        {

            GameManager.Instance.Lose(this.gameObject);
            this.gameObject.SetActive(false);
        }
    }

    IEnumerator Shoot()
    {
        canShoot = false;

        GameObject bullet = GameManager.Instance.BulletRequest();
        bullet.transform.position = canon.position;
        bullet.transform.rotation = transform.rotation;
        yield return new WaitForSecondsRealtime(FireRate);
        canShoot = true;
    }
}

public enum ExitInteraction { Bumper, Wrap, Wall }

