using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Walls
{
    [SerializeField] BoxCollider2D Up;
    [SerializeField] BoxCollider2D Down;
    [SerializeField] BoxCollider2D Right;
    [SerializeField] BoxCollider2D Left;

    public void SetPositionAndSize(float xSize, float ySize)
    {
        Up.gameObject.transform.position += Vector3.up * ySize;
        Down.gameObject.transform.position += Vector3.up * -ySize;

        Right.gameObject.transform.position += Vector3.right * xSize;
        Left.gameObject.transform.position += Vector3.right * -xSize;
    }

    public void TurnOnOff(bool status)
    {
        Up.gameObject.SetActive(status);
        Down.gameObject.SetActive(status);
        Right.gameObject.SetActive(status);
        Left.gameObject.SetActive(status);
    }
}

public class ScreenZone : MonoBehaviour
{
    #region Singleton
    static ScreenZone instance;
    public static ScreenZone Instance { get { return instance; } }

    void Awake()
    {
        instance = this;
    }
    #endregion Singleton
    [SerializeField, Range(0, 2.5f)] float offsetBounds;
    BoxCollider2D boxCol;
    public BoxCollider2D BoxCol { get { return boxCol; } }
    public float OffsetBounds { get { return offsetBounds; } }

    [SerializeField] Walls wallsForWallMode;

    void Start()
    {
        boxCol = GetComponent<BoxCollider2D>();
        float x = (Camera.main.aspect * Camera.main.orthographicSize * 2) - offsetBounds;
        float y = (Camera.main.orthographicSize * 2) - offsetBounds;
        boxCol.size = new Vector2(x, y);

        SetWalls();
    }

    void SetWalls()
    {
        float x = Camera.main.aspect * Camera.main.orthographicSize;
        float y = Camera.main.orthographicSize;
        wallsForWallMode.SetPositionAndSize(x, y);
    }

    public void ChangeWallMode(bool status)
    {
        wallsForWallMode.TurnOnOff(status);
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(transform.position, new Vector3((Camera.main.aspect * Camera.main.orthographicSize * 2) - offsetBounds, (Camera.main.orthographicSize * 2) - offsetBounds, 0));
    }
}
