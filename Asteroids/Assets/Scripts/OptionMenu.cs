using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OptionMenu : MonoBehaviour
{
    [SerializeField] GameObject optionMenu;
    

    public void ChangeToggleWallMode(int type)
    {
        ExitInteraction ei = (ExitInteraction)type;
        Player.Instance.SetExitScreenInteraction(ei);

        if (ei == ExitInteraction.Wall) ScreenZone.Instance.ChangeWallMode(true);
        else ScreenZone.Instance.ChangeWallMode(false);        
    }

    public void ChangeToggleBulletMode(bool status)
    {
        Bullet.SetSine = status;
    }

    public void ChangeRespawnTime(string txt)
    {
        GameManager.Instance.RespawnTime = float.Parse(txt);
    }

    public void OptionsButton()
    {
        optionMenu.SetActive(!optionMenu.activeSelf);
    }
}
