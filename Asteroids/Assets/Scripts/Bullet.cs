
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float speed;
    [SerializeField] float sineAmmount;
    static int sineActive = 0;
    [SerializeField] float damage;

    public static bool SetSine { set { sineActive = value ? 1 : 0; } }
    public float Damage { get => damage; }

    private void Start()
    {
        Init();
    }

    void Init()
    {
        speed = GameManager.Instance.Stats.SpeedBullet;
        damage = GameManager.Instance.Stats.DamageBullet;
        sineAmmount = GameManager.Instance.Stats.SineAmmount;
    }

    private void Update()
    {
        Vector3 direccion = new Vector3(Mathf.Sin(Time.time * sineAmmount * sineActive), 1) * Time.deltaTime * speed;
        transform.Translate(direccion);
    }

    private void OnBecameInvisible()
    {
        this.gameObject.SetActive(false);
    }


}
